# Project ICT - Climate control

## Setup help

### Prerequisites
SERVER
- install docker & docker-compose
	- with 'sudo apt install docker docker-compose -y' on debian based systems like Ubuntu	
- Configure docker-compose.yml 
	- => set values for xxxx
	- => set ports as desired
 
A CLIENT LAPTOP
- install Ansible
	- with 'sudo apt install ansible' on debian based systems like Ubuntu
- add all rpi's to hosts file, with room number between [] 

RPI
- Sensors connected
- static IP preferably
- ssh access with ssh-key
	- => run 'ssh-keygen' && 'ssh-copy-id pi@ipadress' 
### Installation
WEBSITE/BACKEND/DATABASE
1) Place docker-compose.yml on the server
2) SSH into server directory & run 'docker-compose up -d'
3) run 'docker exec app php artisan migrate --seed --force' to initialize the database (only the very first time)

SENSORS
1) enter backend ip + port or domain && SECRET_KEY as in docker-compose.yml in sensor.env
2) run 'ansible-playbook -i ./hosts  sensors.yml'